# IOTA Crypto Core - Gateway PCB

Board-files for Gateway of IOTA Crypto Core Project
Fourth milestone of the ICCFPGA-Projekt: https://gitlab.com/iccfpga/iccfpga-core/wikis/home

The SoM can do the following:

- "Motherboard" for IOTA Crypto Core System Module
- 10/100MB LAN connector
- HS USB Host + Device
- µSD-Card slot
- G3 mobile internet (ublox sara g340)
- WIFI + Bluetooth (Microchip's ATWILC3000)
- 8Pin Mini-Din connector for e.g. Lora Concentrator (SPI and I2C - configurable via Linux Device Tree)
- IRDA

Note: JLCPCB can produce the board via the Gerber files in the repository. It's important to select impedance controlled (JLC2313).

# License
This project is licensed under the MIT-License (https://opensource.org/licenses/MIT)

# Misc

Donations are always welcomed :)

IOTA: `LLEYMHRKXWSPMGCMZFPKKTHSEMYJTNAZXSAYZGQUEXLXEEWPXUNWBFDWESOJVLHQHXOPQEYXGIRBYTLRWHMJAOSHUY`

IOTA Discord: pmaxuw#8292